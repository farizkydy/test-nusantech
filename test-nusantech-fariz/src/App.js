import React, { useState } from "react";
import "./App.css";

function App() {
  
  const [currentSum, setCurrentSum] = useState();
  const [clean, setClean] = useState(false);

  const [state, setState] = useState({
    first: null,
    second: null,
    third: null,
  });

  const handler = (key, value) => {
    const newState = { ...state };
    newState[key] = parseInt(value);
    setState(newState);
  };

  const add = (e) => {
    e.preventDefault();
    let sum = state.first + state.second + state.third;

    setCurrentSum(sum);
  };

  const minus = (e) => {
    e.preventDefault();
    let min = state.first - state.second - state.third;

    setCurrentSum(min);
  };

  const divided = (e) => {
    e.preventDefault();
    let dive = state.first / state.second / state.third;

    setCurrentSum(dive);
  };

  const times = (e) => {
    e.preventDefault();
    let time = state.first * state.second * state.third;

    setCurrentSum(time);
  };

  const cc = (e) => {
    console.log(clean)
    e.preventDefault();
    console.log("sum:", currentSum);
    document.querySelector("form").reset();
    setClean(true);
    setCurrentSum();
  };


  return (
    <div className="App">
      <div className="site-container">
        <div className="title">
          <h3 className="title-paragraph">Calculator</h3>
        </div>
        <form>
          <div className="input-form input-form-1">
            <input
              // ref={refFirst}
              onInput={(e) => handler("first", e.target.value)}
              type="number"
              className="number-input"
              placeholder="enter a number..."
            />
            <input type="checkbox" className="checkbox" checked />
          </div>
          <div className="input-form input-form-2">
            <input
              onInput={(e) => handler("second", e.target.value)}
              type="number"
              className="number-input"
              placeholder="enter a number..."
            />
            <input type="checkbox" className="checkbox" checked />
          </div>
          <div className="input-form input-form-3">
            <input
              onInput={(e) => handler("third", e.target.value)}
              type="number"
              className="number-input"
              placeholder="enter a number..."
            />
            <input type="checkbox" className="checkbox" checked />
          </div>
        </form>
        <div className="operator">
          <button className="btn btn-num-operator" onClick={add}>
            +
          </button>
          <button className="btn btn-num-operator" onClick={minus}>
            -
          </button>
          <button className="btn btn-num-operator" onClick={times}>
            x
          </button>
          <button className="btn btn-num-operator" onClick={divided}>
            /
          </button>
          <button className="btn btn-num-operator" onClick={cc}>
            clear
          </button>
        </div>
        <div className="result">
          <span className="result-text">
            Hasil: <span id="result">{currentSum}</span>
          </span>
        </div>
      </div>
    </div>
  );
}

export default App;
